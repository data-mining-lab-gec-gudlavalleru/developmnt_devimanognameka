
function check()
{
    var n = document.getElementById("count").value;
    var flag=0;
    if(n==4) flag+=1 ;
    else alert("Count the number of elements correctly");

     var a = document.getElementsByClassName("xy");
     var b = document.getElementsByClassName("xx");
     var c = document.getElementsByClassName("yy");

    var xy=[],xx=[],yy=[] ;
    for(i=0;i<4;i++) xy[i]=a[i].value;
    for(i=0;i<4;i++) xx[i]=b[i].value;
    for(i=0;i<4;i++) yy[i]=c[i].value;

   
    if(xy[0]!=37.8 || xy[1] != 34 || xy[2] != 33 || xy[3] != 46.2) alert("X*Y values are incorrect ") ;
    else flag+=1;

    if(xx[0]!=324 || xx[1]!= 289 || xx[2]!= 484 || xx[3]!=441) alert("X*X values are incorrect");
    else flag+=1; 
    
    if(yy[0]!=4.41 || yy[1]!=4 || yy[2]!= 2.25 || yy[3]!= 4.84) alert("Y*Y values are incorrect");
    else flag+=1; 
  
    
    if(flag==4)  {
        document.getElementById("div").style.display="block";
        window.scrollBy(0,500);
    }
    else document.getElementById("div").style.display="none";
    

 }

function verify() 
{
    var flag=0;
    var i = document.getElementsByClassName("inp");
    var inp=[];
    for(j=0;j<5;j++) inp[j]=i[j].value;

    if(inp[0]!=78 ) alert('\u03A3'+"X value is incorrect");
    else  flag+=1;

    if(inp[1]!=7.8 ) alert('\u03A3'+"Y value is incorrect");
    else  flag+=1;

    if(inp[2]!=151 ) alert('\u03A3'+"XY value is incorrect");
    else  flag+=1;
    
    if(inp[3]!=1538 ) alert('\u03A3'+"(X*X) value is incorrect");
    else  flag+=1;

    if(inp[4]!=15.5 ) alert('\u03A3'+"(Y*Y) value is incorrect");
    else  flag+=1;

    var cor = document.getElementById("correlation").value;
    if(cor==-0.495) flag+=1;
    else alert("Correlation value is incorrect");

    if(flag==6) alert("Good Job!!!");
    
   }