function means()
{
	//console.log("gdkjs");
	if(f1.xmean.value=="" || f1.ymean.value=="")
	{
		alert("Please enter all mean values...");
		return false;
	}
	else
	{
		if(f1.xmean.value=="9.1")
		{
			document.getElementById("x").innerHTML="✔";
			x.style.color="green";
			x.style.fontSize="20px";
			temp=1;
		}
		if(f1.ymean.value=="55.4")
		{
			document.getElementById("y").innerHTML="✔";
			y.style.color="green";
			y.style.fontSize="20px";
			temp=1;
		}
		if(f1.xmean.value!="9.1")
		{
			document.getElementById("x").innerHTML="✘";
			x.style.color="red";
			x.style.fontSize="20px";
			x.style.fontWeight="bolder";
			temp=0;
		}
		if(f1.ymean.value!="55.4")
		{
			document.getElementById("y").innerHTML="✘";
			y.style.color="red";
			y.style.fontSize="20px";
			y.style.fontWeight="bolder";
			temp=0;
		}
		if(temp==1)
		{
			document.getElementById("x").innerHTML="✔";
			x.style.color="green";
			x.style.fontSize="20px";
			document.getElementById("y").innerHTML="✔";
			y.style.color="green";
			y.style.fontSize="20px";
			t=document.querySelector(".table");
			t.style.display="block";
			document.querySelector("#xy").remove();
			return true;
		}
		else if(temp==0) {
			alert("Please Try Again.....");
			return false;
		}
	}
}
function first() {
	document.getElementById('one').innerHTML = f1.t3.value;
}
function second() {
	document.getElementById('two').innerHTML = f1.t8.value;
}
function third() {
	document.getElementById('three').innerHTML = f1.t13.value;
}
function fourth() {
	document.getElementById('four').innerHTML = f1.t18.value;
}
function fifth() {
	document.getElementById('five').innerHTML = f1.t23.value;
}
function sixth() {
	document.getElementById('six').innerHTML = f1.t28.value;
}
function seventh() {
	document.getElementById('seven').innerHTML = f1.t33.value;
}
function eighth() {
	document.getElementById('eight').innerHTML = f1.t38.value;
}
function ninth() {
	document.getElementById('nine').innerHTML = f1.t43.value;
}
function tenth() {
	document.getElementById('ten').innerHTML = f1.t48.value;
}
function fir() {
	document.getElementById('o').innerHTML = f1.t4.value;
}
function sec() {
	document.getElementById('t').innerHTML = f1.t9.value;
}
function thi() {
	document.getElementById('th').innerHTML = f1.t14.value;
}
function fo() {
	document.getElementById('f').innerHTML = f1.t19.value;
}
function fif() {
	document.getElementById('fi').innerHTML = f1.t24.value;
}
function si() {
	document.getElementById('s').innerHTML = f1.t29.value;
}
function sev() {
	document.getElementById('se').innerHTML = f1.t34.value;
}
function eig() {
	document.getElementById('e').innerHTML = f1.t39.value;
}
function nin() {
	document.getElementById('n').innerHTML = f1.t44.value;
}
function tent() {
	document.getElementById('te').innerHTML = f1.t49.value;
}
function b_value() {
	document.getElementById('bvalue').innerHTML = f1.b3.value;
}
function ab() {
	document.getElementById('numerator').innerHTML = f1.abc.value;
}
function xyzz() {
	document.getElementById('denomenator').innerHTML = f1.xyz.value;
}
function find_b_value() {
	if(f1.abc.value == '' || f1.xyz.value == '' || f1.b3.value == '') {
		alert("Please Enter all the Values.....");
		return false;
	}
	else {
		if(f1.abc.value == '1269.6') {
			document.getElementById("b1").innerHTML="✔";
			b1.style.color="green";
			b1.style.fontSize="20px";
			temp = 1;
		}
		if(f1.xyz.value == '358.9') {
			document.getElementById("b2").innerHTML="✔";
			b2.style.color="green";
			b2.style.fontSize="20px";
			temp = 1;
		}
		if(f1.b3.value == '3.5') {
			document.getElementById("bval").innerHTML="✔";
			bval.style.color="green";
			bval.style.fontSize="20px";
			temp = 1;
		}
		if(f1.abc.value != '1269.6') {
			document.getElementById("b1").innerHTML="✘";
			b1.style.color="red";
			b1.style.fontSize="20px";
			b1.style.fontWeight="bolder";
			temp = 0;
		}
		if(f1.xyz.value != '358.9') {
			document.getElementById("b2").innerHTML="✘";
			b2.style.color="red";
			b2.style.fontSize="20px";
			b2.style.fontWeight="bolder";
			temp = 0;
		}
		if(f1.b3.value != '3.5') {
			document.getElementById("bval").innerHTML="✘";
			bval.style.color="red";
			bval.style.fontSize="20px";
			bval.style.fontWeight="bolder";
			temp = 0;
		}
		if(temp == 0) {
			alert("Please Try Again.....");
			return false;
		}
		else if(temp == 1) {
			t=document.querySelector(".a");
			t.style.display="block";
			document.querySelector("#bvalu").remove();
			return true;
		}
	}
}
function bary() {
	document.getElementById('bar1').innerHTML = f1.ybar.value;
}
function barb() {
	document.getElementById('barbar').innerHTML = f1.bb.value;
}
function barx() {
	document.getElementById('bar2').innerHTML = f1.xbar.value;
}
function av() {
	document.getElementById('aval').innerHTML = f1.avalue.value;
}
function a_value() {
	if(f1.ybar.value == '' || f1.bb.value == '' || f1.xbar.value == '' || f1.avalue.value == '') {
		alert("Please Enter all the Values.....");
		return false;
	}
	else {
		if(f1.ybar.value == '55.4') {
			document.getElementById("ba1").innerHTML="✔";
			ba1.style.color="green";
			ba1.style.fontSize="20px";
			temp = 1;
		}
		if(f1.bb.value == '3.5') {
			document.getElementById("ba2").innerHTML="✔";
			ba2.style.color="green";
			ba2.style.fontSize="20px";
			temp = 1;
		}
		if(f1.xbar.value == '9.1') {
			document.getElementById("ba3").innerHTML="✔";
			ba3.style.color="green";
			ba3.style.fontSize="20px";
			temp = 1;
		}
		if(f1.avalue.value == '23.6') {
			document.getElementById("ba4").innerHTML="✔";
			ba4.style.color="green";
			ba4.style.fontSize="20px";
			temp = 1;
		}
		if(f1.ybar.value != '55.4') {
			document.getElementById("ba1").innerHTML="✘";
			ba1.style.color="red";
			ba1.style.fontSize="20px";
			ba1.style.fontWeight="bolder";
			temp = 0;
		}
		if(f1.bb.value != '3.5') {
			document.getElementById("ba2").innerHTML="✘";
			ba2.style.color="red";
			ba2.style.fontSize="20px";
			ba2.style.fontWeight="bolder";
			temp = 0;
		}
		if(f1.xbar.value != '9.1') {
			document.getElementById("ba3").innerHTML="✘";
			ba3.style.color="red";
			ba3.style.fontSize="20px";
			ba3.style.fontWeight="bolder";
			temp = 0;
		}
		if(f1.avalue.value != '23.6') {
			document.getElementById("ba4").innerHTML="✘";
			ba4.style.color="red";
			ba4.style.fontSize="20px";
			ba4.style.fontWeight="bolder";
			temp = 0;
		}
		if(temp == 0) {
			alert("Please Try Again.....");
			return false;
		}
		else if(temp == 1) {
			t=document.querySelector(".con");
			t.style.display="block";
			document.querySelector("#avalu").remove();
			return true;
		}
	}
}
function aaval() {
	document.getElementById('aaaa').innerHTML = f1.aaa.value;
	aaaa.style.fontWeight = "bolder";
	aaaa.style.color = "maroon";
	aaaa.fontSize = "20px";
}
function bbval() {
	document.getElementById('bbbb').innerHTML = f1.bbb.value;
	bbbb.style.fontWeight = "bolder";
	bbbb.style.color = "maroon";
	bbbb.style.fontSize = "20px";
}
