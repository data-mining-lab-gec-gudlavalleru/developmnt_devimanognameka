var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}




var tp = parseInt(document.getElementById("tp").innerHTML);
var fn = parseInt(document.getElementById("fn").innerHTML);
var fp = parseInt(document.getElementById("fp").innerHTML);
var tn = parseInt(document.getElementById("tn").innerHTML);


function check1()
{
  var i1 = document.getElementById("i1").value;
  var i2 = document.getElementById("i2").value;
  var l1 = document.getElementById("l1");
  var l2 = document.getElementById("l2");

  var accuracy = ((tp+tn)/(tp+tn+fp+fn)).toFixed(2);  
  var error_rate = (1-accuracy).toFixed(2);
  

  if(i1 == accuracy && i2 == error_rate)   document.getElementById("d1").style.display="block"; 
  if(i1 != accuracy || i2 != error_rate)   document.getElementById("d1").style.display="none"; 
  
  if(i1 == accuracy){
	  l1.innerHTML = "&#10004;&#65039;";
	  l1.style.color = "green";
  }
  else {
	  l1.innerHTML = "&#10060 Try again";
	  l1.style.color = "red";
  }
  if(i2 == error_rate) {
	  l2.innerHTML = "&#10004;&#65039;";
	  l2.style.color = "green";
  }
  else {
	  l2.innerHTML = "&#10060 Try again";
	  l2.style.color = "red";
  }
}
function check2()
{
  var precission = (tp/(tp+fp)).toFixed(2);
  var recall = (tp/(tp+fn)).toFixed(2);

  var i3 = document.getElementById("i3").value;
  var i4 = document.getElementById("i4").value;
  var l3 = document.getElementById("l3");
  var l4 = document.getElementById("l4");
  
  if(i3 == precission) {
	  l3.innerHTML = "&#10004;&#65039;";
	  l3.style.color = "green";
  }
  else {
	  l3.innerHTML = "&#10060 Try again";
	  l3.style.color = "red";
  }
  if(i4 == recall) {
	  l4.innerHTML = "&#10004;&#65039;";
	  l4.style.color = "green";
  }
  else {
	  l4.innerHTML = "&#10060 Try again";
	  l4.style.color = "red";
  }
  if(i3 == precission && i4 == recall)   alert("Good Job !!! ");
}
