
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var d = document.getElementById("d");
// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
  d.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  d.style.display = "block";

}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    d.style.display = "block";
  }
}



    function dragStart(ev) {
            ev.dataTransfer.effectAllowed='move';
            ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));
            ev.dataTransfer.setDragImage(ev.target,0,0);
            return true;
    }
function drop(event) {
event.preventDefault();
var data = event.dataTransfer.getData("Text");
event.target.appendChild(document.getElementById(data));
}
function allowDrop(event) {
    event.preventDefault();
}

function check()
{
    var d11=document.getElementById("d11").textContent;
    var d12=document.getElementById("d12").textContent;
    var d13=document.getElementById("d13").textContent;
    var d14=document.getElementById("d14").textContent;
    var d15=document.getElementById("d15").textContent;
    var d16=document.getElementById("d16").textContent;
    var d17=document.getElementById("d17").textContent;
    var d18=document.getElementById("d18").textContent;
    var d19=document.getElementById("d19").textContent;

    if((d11==0) || (d12==0) || (d13==0) || (d14==0) || (d15==0) || (d16==0) || (d17==0) || (d18==0) || (d19==0) ) alert("Please fill all the boxes");

    else if( (d11<d12) && (d12<d13) && (d13<d14) && (d14<d15) && (d15<d16) && (d16<d17) && (d17<d18) && (d18<d19))
    {
       
        let v=document.querySelector(".bin")
        v.style.display="block";
        document.querySelector("#check").remove();
    } 
    else{
        alert("Please sort the elements correctly");
		return false;
    }

}

function verify()
{
    var b1 = document.getElementById("b1").textContent;
    var b2 = document.getElementById("b2").textContent;
    var b3 = document.getElementById("b3").textContent;
    var b4 = document.getElementById("b4").textContent;
    var b5 = document.getElementById("b5").textContent;
    var b6 = document.getElementById("b6").textContent;
    var b7 = document.getElementById("b7").textContent;
    var b8 = document.getElementById("b8").textContent;
    var b9 = document.getElementById("b9").textContent;

    if(b1==0 || b2==0 || b3==0 || b4==0 || b5==0 || b6==0 || b7==0 || b8==0 || b9==0) {
		alert("Please fill all the boxes");
		return false;
	}
    else if((b1<b2) && (b2<b3) && (b3<b4) && (b4<b5) && (b5<b6) && (b6<b7) && (b7<b8) && (b8<b9)){
		if(form.bin1.value == "" || form.bin2.value == ""|| form.bin3.value == "") {
			alert("Please enter means of all the bins");
			return false;
		}
		else {
			mean1 = parseInt((1 + 2 + 3) / 3);
			mean2 = parseInt((4 + 5 + 6) / 3);
			mean3 = parseInt((7 + 8 + 9) / 3);
		}
		if(mean1 == form.bin1.value) {
			document.getElementById("one").innerHTML = "&#10003;";
			one.style.color = "green";
			one.style.fontSize = "20px";
			one.style.fontWeight = "bolder";
			temp = 1;
		}
		if(mean2 == form.bin2.value) {
			document.getElementById("two").innerHTML = "&#10003;";
			two.style.color = "green";
			two.style.fontSize = "20px";
			two.style.fontWeight = "bolder";
			temp = 1;
		}
		if(mean3 == form.bin3.value) {
			document.getElementById("three").innerHTML = "&#10003;";
			three.style.color = "green";
			three.style.fontSize = "20px";
			three.style.fontWeight = "bolder";
			temp = 1;
		}
		if(mean1 != form.bin1.value) {
			document.getElementById("one").innerHTML = "&#10060;";
			one.style.color = "red";
			one.style.fontSize = "20px";
			one.style.fontWeight = "bolder";
			temp = 0;
		}
		if(mean2 != form.bin2.value) {
			document.getElementById("two").innerHTML = "&#10060;";
			two.style.color = "red";
			two.style.fontSize = "20px";
			two.style.fontWeight = "bolder";
			temp = 0;
		}
		if(mean3 != form.bin3.value) {
			document.getElementById("three").innerHTML = "&#10060;";
			three.style.color = "red";
			three.style.fontSize = "20px";
			three.style.fontWeight = "bolder";
			temp = 0;
		}
		if(temp == 1) {
			document.querySelector(".LastOne").remove()
			document.getElementById("last_div").style.display="block";
			return false;
		}
		else if(temp == 0) {
			
			return false;
		}
	}
    else {
		alert("Please arrange the elements correctly");
		return false;
	}
}



function smoothing(){
	var sm1=document.getElementsByClassName("smoothing1");
	var sm2=document.getElementsByClassName("smoothing2");
	var sm3=document.getElementsByClassName("smoothing3");
	var flag=0;
	if(sm1[0].value=="2" && sm1[1].value=="2" && sm1[2].value=="2"){
		flag+=1;
		document.getElementById("span1").innerHTML=" &#10004;&#65039;";
	}
	else{
		document.getElementById("span1").innerHTML="&#10060";
	}

	if(sm2[0].value=="5" && sm2[1].value=="5" && sm2[2].value=="5"){
		flag+=1;
		document.getElementById("span2").innerHTML=" &#10004;&#65039;";
	}
	else{
		document.getElementById("span2").innerHTML="&#10060";
	}
	if(sm3[0].value=="8" && sm3[1].value=="8" && sm3[2].value=="8"){
		flag+=1;
		document.getElementById("span3").innerHTML=" &#10004;&#65039;";
	}
	else{
		document.getElementById("span3").innerHTML="&#10060";
	}
	if(flag==3) alert("Well Done!!...");
}