function mean() {
	if(m.n1.value == "" || m.t1.value == "" || m.mean1.value == "") {
		alert("Enter all the values.....");
		return false;
	}
	else {
		if(m.mean1.value == "20") {
			document.getElementById("m1").innerHTML = "&#10003;";
			m1.style.color = "green";
			m1.style.fontSize = "20px";
			m1.style.fontWeight = "bolder";
			t=document.querySelector(".mean");
			t.style.display="block";
			t1 = document.querySelector(".mode");
			t1.style.display="block";
			document.querySelector("#c1").remove();
			return true;
		}
		else {
			document.getElementById("m1").innerHTML = "&#10060;";
			m1.style.color = "red";
			m1.style.fontSize = "20px";
			m1.style.fontWeight = "bolder";
			alert("Please try again.....");
			return false;
		}
	}
}
function mode()
	{
		if(mo.mode1.value == "") {
		alert("Enter mode value.....");
		return false;
	}
	else {
		if((mo.mode1.value).toLowerCase()=="female") {
			document.getElementById("m2").innerHTML = "&#10003;";
			m2.style.color = "green";
			m2.style.fontSize = "20px";
			m2.style.fontWeight = "bolder";
			mo2 = document.querySelector(".modeans");
			mo2.style.display="block";
			g = document.querySelector(".global");
			g.style.display="block";
			g1 = document.querySelector(".classmean");
			g1.style.display="block";
			document.querySelector("#c2").remove();
			return true;
		}
		else {
			document.getElementById("m2").innerHTML = "&#10060;";
			m2.style.color = "red";
			m2.style.fontSize = "20px";
			m2.style.fontWeight = "bolder";
			alert("Please try again.....");
			return false;
		}
	}
}
function abclassmean() {
	if(c.a1.value == ""||c.a2.value == ""||c.a.value == ""||c.b1.value==""||c.b2.value==""||c.b.value=="") {
		alert("Enter all the values.....");
		return false;
	}
	else {
		if(c.a.value == "20.5") {
			document.getElementById("amean").innerHTML = "&#10003;";
			amean.style.color = "green";
			amean.style.fontSize = "20px";
			amean.style.fontWeight = "bolder";
		}
		if(c.b.value == "19") {
			document.getElementById("bmean").innerHTML = "&#10003;";
			bmean.style.color = "green";
			bmean.style.fontSize = "20px";
			bmean.style.fontWeight = "bolder";
		}
		if(c.a.value != "20.5") {
			document.getElementById("amean").innerHTML = "&#10060;";
			amean.style.color = "red";
			amean.style.fontSize = "20px";
			amean.style.fontWeight = "bolder";
		}
		if(c.b.value != "19") {
			document.getElementById("bmean").innerHTML = "&#10060;";
			bmean.style.color = "red";
			bmean.style.fontSize = "20px";
			bmean.style.fontWeight = "bolder";
		}
		if(c.a.value=="20.5" && c.b.value=="19") {
			t=document.querySelector(".classmean1");
			t.style.display="block";
			t1 = document.querySelector(".ignore");
			t1.style.display="block";
			document.querySelector("#ab").remove();
			return true;
		}
		if(c.a.value!="20.5" || c.b.value!="19"){
			alert("Please Try Again.....");
			return false;
		}
	}
}